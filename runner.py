import numpy as np
import subprocess

def get_forces(positions, lattice, elements):
    write_input('input.data', 'w', positions, lattice, elements)
    write_input('traj.data', 'a', positions, lattice, elements)
    subprocess.run(['./RuNNer.x > /dev/null'], shell=True)
    forces = read_forces()
    return forces

def read_data(filename):
    lattice   = []
    positions = []
    elements  = []
    with open(filename, 'r') as file:
        lines = file.readlines()
    for line in lines:
        if line.startswith('lattice'):
            lattice.append(list(map(float, line.split()[1:4])))
        elif line.startswith('atom'):
            positions.append(list(map(float, line.split()[1:4])))
            elements.append(line.split()[4])

    return np.array(positions), np.array(lattice), elements

def write_input(filename, mode, positions, lattice, elements):
    with open(filename, mode) as file:
        file.write("begin\n")
        for vec in lattice:
            file.write(f"lattice {vec[0]} {vec[1]} {vec[2]}\n")
        for i, pos in enumerate(positions):
            file.write(f"atom {pos[0]} {pos[1]} {pos[2]} {elements[i]} 0.0 0.0 0.0 0.0 0.0\n")  # Simplified atom line
        file.write("energy 0\n")
        file.write("charge 0\n")
        file.write("end\n")

def read_forces():
    with open('nnforces.out', 'r') as file:
        lines = file.readlines()
    forces = []
    for line in lines[1:]:
        forces.append(list(map(float, line.split()[5:8])))
    return np.array(forces)

def read_energy():
    with open('energy.out', 'r') as file:
        lines = file.readlines()
    return float(lines[1].split()[3])
