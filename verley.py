#!/usr/bin/env python3
import numpy as np
import runner

kB = 3.166811429e-6  # Hartree/Kelvin

# Verlet integration
def velocity_verlet(positions, velocities, lattice, dt, steps, elements, constraints=[]):

    nat = len(elements)
    n_constraints = np.zeros(3)
    for constraint in constraints:
        n_constraints += 1 - constraint[1:4]
    dof_xyz = nat - n_constraints
    dof_tot = np.sum(dof_xyz)

    accelerations = constrained_acceleration(positions, lattice, elements, constraints)

    for step in range(steps):
        # Verlet integration
        positions  += velocities * dt + 0.5 * accelerations * dt**2
        accelerations_new = constrained_acceleration(positions, lattice, elements, constraints)
        velocities += 0.5 * (accelerations + accelerations_new) * dt
        accelerations = accelerations_new

        # Avoid drift in the system by removing the center of mass velocity
        velocities -= np.sum(velocities, axis=0) / dof_xyz

        for constraint in constraints:
            velocities[constraint[0]] *= constraint[1:4]
        
        k = 0.5 * np.sum(velocities**2)
        v = runner.read_energy()
        e_tot = k + v
        temperature = 2 * k / (dof_tot * kB)
        print(f"Step {step+1}, E_total = {e_tot}, T = {temperature}, V = {v}, K = {k}")

def constrained_acceleration(positions, lattice, elements, constraints):
    forces = runner.get_forces(positions, lattice, elements) # in atomic units
    accelerations = forces  # assuming mass = 1 and direct unit use
    for constraint in constraints:
        accelerations[constraint[0]] *= constraint[1:4]
    return accelerations

def initialize_velocities(num_particles, temperature, constraints, mass=1):
    # Mean zero, stddev from equipartition theorem: (k_B T / m)^0.5
    stddev = np.sqrt(kB * temperature / mass)
    
    velocities = np.random.normal(0, stddev, (num_particles, 3))

    n_constraints = np.zeros(3)
    for constraint in constraints:
        n_constraints += 1 - constraint[1:4]
        velocities[constraint[0]] *= constraint[1:4]
    
    # Correct for momentum conservation: zero the total momentum
    total_momentum = np.sum(velocities, axis=0)
    velocities -= total_momentum / (num_particles - n_constraints)

    for constraint in constraints:
        velocities[constraint[0]] *= constraint[1:4]

    return velocities

constraints_tmp = []
with open('md.in', 'r') as file:
    lines = file.readlines()
for line in lines:
    if line.startswith('dt'):
        dt = float(line.split()[1]) # Time step in atomic units
    elif line.startswith('steps'):
        steps = int(line.split()[1])  # Total number of simulation steps
    elif line.startswith('temperature'):
        temperature = float(line.split()[1]) # Kelvin
    elif line.startswith('constraint'):
        constraints_tmp.append(list(map(int,line.split()[1:5])))
    elif line.startswith('#'):
        pass
    else:
        print(f"Unknown line: {line}")
        quit()

constraints = np.array(constraints_tmp)
positions, lattice, elements = runner.read_data('md_start.data')
velocities = initialize_velocities(len(elements), temperature, constraints)
velocity_verlet(positions, velocities, lattice, dt, steps, elements, constraints)
